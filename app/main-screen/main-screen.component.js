'use strict';

angular
  .module('notesApp')
  .component('mainScreen', {
    templateUrl: 'main-screen/main-screen.template.html',
    controller: [
      '$scope',
      'NotesService',
      WrapperController
    ]
  });

function WrapperController($scope, NotesService) {
  $scope.view = 'cover';
  $scope.$on('change-view', function (event, viewName) {
    $scope.view = viewName;
  });

  $scope.$on('delete-note', function (event, note) {
    const isDeletingAllow = confirm(`Do you really want to delete a note ${$scope.getNoteName(note)}?`);
    if (isDeletingAllow) {
      NotesService.DeleteNote(note.id);
      if ($scope.clickedNote && $scope.clickedNote.id === note.id && $scope.view === 'note') {
        $scope.view = 'cover';
      }
    }
  });

  $scope.getNoteName = function (note) {
    const shortNoteText = (note.text.length > 6) ? `${note.text.slice(0, 7)}...` : note.text;
    return note.title ? note.title : shortNoteText;
  };
}
