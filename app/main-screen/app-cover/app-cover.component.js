'use strict';

angular
  .module('mainScreen')
  .component('appCover', {
    templateUrl: 'main-screen/app-cover/app-cover.template.html',
    controller: [
      '$scope',
      AppCoverController
    ]
  });

function AppCoverController($scope) {

}
