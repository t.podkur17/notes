'use strict';

angular
  .module('mainScreen')
  .component('notesList', {
    templateUrl: 'main-screen/notes-list/notes-list.template.html',
    controller: [
      '$scope',
      '$rootScope',
      'NotesService',
      NotesListController
    ],
    bindings: {
      clickedNote: '=',
    }
  });

function NotesListController($scope, $rootScope, NotesService) {
  $scope.notes = NotesService.GetNotes();
  $scope.$on('update-notes', function () {
    $scope.notes = NotesService.GetNotes();
  });
  $scope.searchQuery = '';

  $scope.filterNotes = function (note) {
    if (!$scope.searchQuery) return true;
    const lowercaseSearchQuery = $scope.searchQuery.toLowerCase();
    const lowercaseTitle = note.title ? note.title.toLowerCase() : "";
    const lowercaseText = note.text.toLowerCase();
    return lowercaseTitle.indexOf(lowercaseSearchQuery) >= 0 || (lowercaseText.indexOf(lowercaseSearchQuery) >= 0);
  };

  $scope.deleteNote = function (note) {
    $rootScope.$broadcast('delete-note', note);
  };

  $scope.notesListClick = function () {
    $rootScope.$broadcast('change-view', 'note');
  };

  $scope.$on('save-new-note', function () {
    $scope.searchQuery = '';
  });
}
