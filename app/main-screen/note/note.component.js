'use strict';

angular
  .module('mainScreen')
  .component('note', {
    templateUrl: 'main-screen/note/note.template.html',
    controller: [
      '$scope',
      '$rootScope',
      NoteController
    ],
    bindings: {
      note: '<'
    }
  });

function NoteController($scope, $rootScope) {
  $scope.deleteNote = function (note) {
    $rootScope.$broadcast('delete-note', note);
  };
}
