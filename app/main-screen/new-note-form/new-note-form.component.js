'use strict';

angular
  .module('mainScreen')
    .component('newNoteForm', {
      templateUrl: 'main-screen/new-note-form/new-note-form.template.html',
      controller: [
        '$scope',
        '$rootScope',
        'NotesService',
        NewNoteFormController
      ],
    });

function NewNoteFormController($scope, $rootScope, NotesService) {
  $scope.save = function($event, newNote) {
    if (newNote.text) {
      NotesService.SaveNote(newNote);
      $scope.newNote = null;
      $event.preventDefault();
      $rootScope.$broadcast('save-new-note');
    }
  };
}
