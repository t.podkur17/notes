'use strict';

angular.
  module('core.notes')
    .factory('NotesService', function($rootScope) {
      return {
        SaveNote: function (newNote) {
          newNote.id = generateId();

          const allNotes = localStorage.notesService ? angular.fromJson(localStorage.notesService) : [];
          allNotes.unshift(newNote);
          localStorage.notesService = angular.toJson(allNotes);

          $rootScope.$broadcast('update-notes');
        },

        GetNotes: function () {
          return angular.fromJson(localStorage.notesService);
        },

        DeleteNote(noteId) {
          const allNotes = angular.fromJson(localStorage.notesService);
          if (allNotes.length) {
            const correctNotes = allNotes.filter(note => note.id !== noteId);
            localStorage.notesService = angular.toJson(correctNotes);
            $rootScope.$broadcast('update-notes');
          }
        }
      }
    }
);

function generateId() {
  return Math.random().toString(36).substr(2, 9);
}
