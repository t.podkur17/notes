# Notes
In the app, you can easily write down small ideas or keep extensive records, and delete them when they are no longer needed. To navigate through a large number of notes, just use the text search.

## Getting started
Before starting the project, you will need to install Node: https://nodejs.org/ <br><br>
To get the project up and running, and view components in the browser, complete the following steps:

```shell
git clone https://gitlab.com/t.podkur17/notes.git
cd notes
npm install
npm start
```
Then open your browser and visit http://localhost:8000 <br><br>

The code above clone this repository, install project dependancies and start the development environment.
